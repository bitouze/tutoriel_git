MD_FILES = $(shell find -name "*.md")
HTML_FILES = $(MD_FILES:.md=.html)

all: $(HTML_FILES)

before.html: before.md
	pandoc --css template.css -o $@ $<

%.html: %.md before.html 
	pandoc --template template.html --css template.css --toc --toc-depth 2 --include-before before.html -o $@ $<

publish:
	scp $(HTML_FILES) *.svg *.png template.* yangra.univ-littoral.fr:public-html/enseignements/tutoriel_git/

clean:
	rm -f $(HTML_FILES)

