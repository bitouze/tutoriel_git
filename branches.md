
---
title: "Branches"
date: 2016-03-25
---

Lorsque l'on fait des commits successifs, ceux-ci sont ajoutés les uns à la
suite des autres, sur une même branche.  Git permet également, à partir d'un
commit donné, de réaliser plusieurs branches en parallèle.  Les branches
peuvent ensuite être fusionnées, abandonnées ou même supprimées.

L'utilisation des branches est très naturelle avec Git et permet d'organiser le
déroulement du projet. Souvent, on utilise une branche principale `master` qui
« doit toujours fonctionner » et, pour chaque fonctionnalité à ajouter, on dérive
une nouvelle branche que l'on fusionne ensuite dans le `master` une fois la
fonctionnalité réalisée (ou partiellement réalisée) et testée.

![](branches_01.svg)

## Afficher les branches 

La commande `git branch` permet d'afficher les branches.

![](branches_01.png)

On peut avoir plus d'information avec la commande `git log --graph --oneline --decorate`.

![](branches_21.png)

Sur le graphe des commits, on indique la branche courant avec un `*`.

![](branches_02.svg)

## Créer une nouvelle branche locale

La command `git branch ...` permet de créer de nouvelles branches.

![](branches_02.png)

![](branches_03.svg)

## Changer de branche

La commande `git checkout ...` permet de sélectionner une branche.

![](branches_03.png)

![](branches_04.svg)

Les nouveaux commits sont alors effectués pour la branche sélectionnée.
Par exemple, si on modifie le fichier `paper.tex` et qu'on commite ensuite :

![](branches_05.png)

![](branches_05.svg)

Le `master` est une branche comme les autres qu'on peut également sélectionner.

![](branches_06.png)

![](branches_06.svg)

## Fusionner des branches

Pour fusionner des branches, il faut d'abord sélectionner la branche qui doit
recevoir les modifications puis utiliser la commande `git merge` en précisant
la branche qu'il faut intégrer dans la branche courante.

![](branches_08.png)

![](branches_07.svg)

Un nouveau commit, correspondant à la fusion, est alors créé. On peut le 
vérifier avec un `git log`...

![](branches_09.png)

... ou avec un client graphique.

![](branches_10.png)

## Envoyer une branche sur un dépôt distant

Lorsque l'on crée une branche, celle-ci est locale au dépôt. Pour la créer et 
l'envoyer également sur le serveur, il faut utiliser la commande `git push
--set-upstream ...`. Ainsi, la branche locale et la branche distante seront
associées et il suffira ensuite d'un push classique de la branche locale pour
l'envoyer sur le serveur.

Par exemple, pour créer et envoyer la branche `intro_papier` sur le dépôt distant `origin` :

![](branches_11.png)

![](branches_08.svg)

On peut également voir sur le site Gogs que la branche a bien été créée sur le 
serveur.

![](branches_12.png)

## Afficher les branches distantes

La commande `git ls-remote` permet de lister les branches distantes.

![](branches_15.png)

## Terminer une branche locale

Pour terminer une branche locale (c'est-à-dire supprimer l'étiquette
correspondante dans le dépôt local), il faut d'abord sélectionner une autre
branche...

![](branches_13a.png)

![](branches_09.svg)

... puis utiliser la commande `git branch -d ...`.

![](branches_13b.png)

![](branches_10.svg)

## Terminer une branche distante

Pour terminer une branche distante (c'est-à-dire supprimer l'étiquette
correspondante sur le serveur), il faut utiliser la commande `git push --delete
...`

![](branches_16.png)

![](branches_11.svg)

## Supprimer un commit déjà pushé (danger !!!)

Généralement, vouloir supprimer un commit déjà envoyé sur le serveur est une
mauvaise idée.  En effet, en plus de perdre les données sauvegardées, cela peut
casser les commits d'un collaborateur qui aurait déjà récupéré et continué les
commits en question, donc il vaut mieux commiter des corrections.

Mais si c'est vraiment ce que vous voulez faire, voici la procédure (pensez
à vérifier que les commits locaux sont également supprimés sinon les commits
supprimés sur le serveur seront de nouveau envoyés au prochain push).

![](branches_18.png)

## Réécrire l'historique

Souvent, on crée des commits au fur et à mesure, pour sauvegarder et partager
les modifications. Mais pour certaines applications, on peut vouloir un graphe
de commits « plus propre », où les commits doivent suivre une structure
prédéterminée, ou au contraire les réorganiser a posteriori. Git offre des
fonctionnalités pour cela : regrouper des commits, les déplacer, etc...  Ces
fonctionnalités ne sont pas traitées dans ce tutoriel (voir la documentation
sur les `git rebase`).


## Résumé et méthode de travail

Résumé des commandes Git précédentes :

---|---|
`git branch` | affiche la liste des branches |
`git log --graph --oneline --decorate` | affiche le graph des commits |
`git ls-remote` | affiche la liste des branches distantes |
`git branch <nom>` | crée une nouvelle branche locale |
`git checkout <branche>` | sélectionne une branche |
`git merge <branche>` | fusionne une branche dans la branche courante |
`git push --set-upstream <remote> <branche>` | envoie la branche courante dans une branche distante |
`git branch -d <branche>` | supprime une branche locale (enlève l'étiquette) |
`git push --delete <remote> <branche>` | supprime une branche distante |

Quelques conseils de méthode de travail :

- Essayez d'avoir une branche `master` « qui marche tout le temps ».
- Pour chaque tâche du projet, créez une branche dédiée, que vous fusionnerez
  dans le `master` une fois les modifications commitées.
- Utiliser des branches ne vaccine pas contre les conflits, donc pensez à faire
  quand même le point avec vos collègues régulièrement.

## Exercice

- Clonez un dépôt distant.
- Créez une nouvelle branche `b1`, faites quelques commits dans `b1` puis
  fusionnez-les dans le master.
- Envoyez la branche `b1` sur le serveur et vérifiez que vous la voyez sur la
  page web du serveur.
- Créez une branche `b2` à partir du commit précédant le master courant, faites
  quelques commits dans `b2` puis fusionnez-les dans le master.
- Fusionnez le master dans `b1` et vérifiez le graphe des commits avec le client
  Git console et avec un client graphique.
- Envoyez la branche `b2` sur le serveur puis supprimez-la sur le dépôt local puis
  sur le dépôt distant.

