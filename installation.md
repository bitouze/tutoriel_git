---
title: "Installation et configuration"
date: 2016-03-25
---

Pour utiliser Git, il vous faut un client Git (et une connexion internet si
vous voulez utiliser des dépôts distants).

## Ligne de commandes vs interface graphique

Il existe deux types de client Git : le client console (en ligne de commandes)
et les [clients
graphiques](https://git-scm.com/book/fr/v2/Git-dans-d%E2%80%99autres-environnements-Interfaces-graphiques)
(par exemple : gitg, giggle, qgit, gitk, git-gui, github-desktop...).

[La communauté conseille souvent d'utiliser le client
console.](https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-La-ligne-de-commande)
Celui-ci est certes moche mais c'est le seul qui donne accès à toutes les
commandes Git. De plus, il est souvent plus efficace, une fois passée la phase
d'apprentissage. 

## Installation sous Linux

La plupart des systèmes Linux modernes propose le client Git dans leur système
de paquets. Par exemple, sur les Linux basés Debian, il suffit de lancer la
commande console :

```
sudo apt-get install git
```

## Installation sous Mac

Télécharger et installer le client [Git pour
Mac](http://git-scm.com/download/mac). Le client console est ensuite accessible
depuis un terminal.

## Installation sous Windows 

Télécharger et installer le client [Git pour
Windows](http://git-scm.com/download/win). Le client console est ensuite
accessible depuis l'outils git-bash inclus (ou depuis powershell pour ceux qui
utilisent cet outil).

## Configuration

Après avoir installé le client Git, il faut configurer le nom et l'adresse
e-mail de l'utilisateur; par exemple, en tapant les commandes consoles : 

```
git config --global user.name "Julien Dehos"
git config --global user.email dehos@nimpe.org
```

On peut également configurer l'éditeur de texte et le proxy que le client Git
doit utiliser; par exemple, en tapant les commandes consoles :

```
git config --global core.editor emacs
git config --global http.proxy http://user:pass@proxyhost:proxyport
```
ou bien en ajoutant des variables d'environnement (par exemple, dans le
`.bashrc`): 
```
export EDITOR=emacs
export http_proxy=http://user:pass@proxyhost:proxyport
```

## Obtenir de l'aide

Aide générale sur Git :
```
git help 
```

Aide sur une commande Git :
```
git help <commande git>
```

Voir également la section [références](index.html#références).

## Exercice

Installez et configurez le client Git sur votre machine.  Vérifiez que votre
client Git peut accéder à l'extérieur, par exemple en récupérant le dépôt
distant `https://github.com/juliendehos/invinoveritas` :

![](installation_01.png)

